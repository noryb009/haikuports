DESCRIPTION="
	Boost is a set of libraries for the C++ programming language that
	provide support for tasks and structures such as linear algebra,
	pseudorandom number generation, multithreading, image processing,
	regular expressions, and unit testing. It contains over eighty
	individual libraries.
	"
SUMMARY="Boost is a set of libraries for the C++ programming language."
HOMEPAGE="http://www.boost.org/" 
SRC_URI="http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.bz2"
CHECKSUM_MD5="d6eef4b4cacb2183f2bf265a5a03a354"
REVISION="1"
LICENSE="Boost v1.0"
COPYRIGHT="1998-2013 Beman Dawes, David Abrahams, Rene Rivera, et al."
ARCHITECTURES="x86 ?x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86 ?x86_64"

SOURCE_DIR="boost_1_55_0"	

PATCHES="boost-1.55.0.patchset"

PROVIDES="
	lib:boost = 1.55.0
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libiconv$secondaryArchSuffix
	lib:libbz2$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libiconv$secondaryArchSuffix
	devel:libbz2$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	boehm_gc
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:jam
	cmd:iconv
	cmd:make
	binutils
	"

BUILD()
{
	./bootstrap.sh \
		--without-icu \
		--prefix=$prefix \
		--exec-prefix=$binDir \
		--libdir=$libDir \
		--includedir=$includeDir
	./bjam \
		 -sICU_PATH=$prefix \
		 -sICONV_PATH=$prefix \
		-d2 \
		--without-mpi \
		--prefix=$prefix \
		--exec-prefix=$binDir \
		--libdir=$libDir \
		--includedir=$includeDir \
		--enable-threads=posix \
		--enable-thread-local-alloc \
 		--enable-parallel-mark \
		inlining=on \
		linkflags=-L$libDir \
		threading=multi \
		variant=release \
		link=shared \
		runtime-link=shared \
		--without-python \
		--disable-icu 
}

INSTALL()
{
	mkdir -p $includeDir
	mkdir -p $libDir
	
	cp -rf boost $includeDir
	cp stage/lib/* $libDir
		
	packageEntries devel $includeDir
	packageEntries atomic $libDir/*atomic*
	packageEntries chrono $libDir/*chrono*
	packageEntries context $libDir/*context*
	packageEntries coroutine $libDir/*coroutine*
	packageEntries date_time $libDir/*date_time*
	packageEntries exception $libDir/*exception*
	packageEntries filesystem $libDir/*filesystem*
	packageEntries graph $libDir/*graph*
	packageEntries iostreams $libDir/*iostreams*
	packageEntries locale $libDir/*locale*
	packageEntries log $libDir/*log*
	packageEntries math $libDir/*math*
	packageEntries program_options $libDir/*program_options*
	packageEntries random $libDir/*random*
	packageEntries regex $libDir/*regex*
	packageEntries serialization $libDir/*serialization*
	packageEntries signals $libDir/*signals*
	packageEntries system $libDir/*system* 
	packageEntries test $libDir/*test*
	packageEntries thread $libDir/*thread*
	packageEntries timer $libDir/*timer*
	packageEntries wave $libDir/*wave*
}

PROVIDES_devel="
	boost_devel$secondaryArchSuffix = $portVersion
	"

REQUIRES_devel="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_atomic="
	boost_atomic$secondaryArchSuffix = $portVersion
	"

REQUIRES_atomic="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_chrono="
	boost_chrono$secondaryArchSuffix = $portVersion
	"

REQUIRES_chrono="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_system$secondaryArchSuffix == $portVersion
	"

PROVIDES_context="
	boost_context$secondaryArchSuffix = $portVersion
	"

REQUIRES_context="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_coroutine="
	boost_coroutine$secondaryArchSuffix = $portVersion
	"

REQUIRES_coroutine="
	boost_context$secondaryArchSuffix == $portVersion
	"

PROVIDES_date_time="
	boost_date_time$secondaryArchSuffix = $portVersion
	"

REQUIRES_date_time="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_exception="
	boost_exception$secondaryArchSuffix = $portVersion
	"

REQUIRES_exception="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_filesystem="
	boost_filesystem$secondaryArchSuffix = $portVersion
	"

REQUIRES_filesystem="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_system$secondaryArchSuffix == $portVersion
	lib:libz$secondaryArchSuffix
	lib:libbz2$secondaryArchSuffix
	"

PROVIDES_graph="
	boost_graph$secondaryArchSuffix = $portVersion
	"

REQUIRES_graph="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_regex$secondaryArchSuffix == $portVersion
	"

PROVIDES_iostreams="
	boost_iostreams$secondaryArchSuffix = $portVersion
	"

REQUIRES_iostreams="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_locale="
	boost_locale$secondaryArchSuffix = $portVersion
	"

REQUIRES_locale="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_chrono$secondaryArchSuffix == $portVersion
	boost_system$secondaryArchSuffix == $portVersion
	boost_thread$secondaryArchSuffix == $portVersion
	"

PROVIDES_log="
	boost_log$secondaryArchSuffix = $portVersion
	"

REQUIRES_log="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_chrono$secondaryArchSuffix == $portVersion
	boost_system$secondaryArchSuffix == $portVersion
	boost_date_time$secondaryArchSuffix == $portVersion
	boost_thread$secondaryArchSuffix == $portVersion
	"

PROVIDES_math="
	boost_math$secondaryArchSuffix = $portVersion
	"

REQUIRES_math="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_program_options="
	boost_program_options$secondaryArchSuffix = $portVersion
	"

REQUIRES_program_options="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_random="
	boost_random$secondaryArchSuffix = $portVersion
	"

REQUIRES_random="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_regex="
	boost_regex$secondaryArchSuffix = $portVersion
	"

REQUIRES_regex="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_serialization="
	boost_serialization$secondaryArchSuffix = $portVersion
	"

REQUIRES_serialization="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_signals="
	boost_signals$secondaryArchSuffix = $portVersion
	"

REQUIRES_signals="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_system="
	boost_system$secondaryArchSuffix = $portVersion
	"

REQUIRES_system="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_test="
	boost_test$secondaryArchSuffix = $portVersion
	"

REQUIRES_test="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

PROVIDES_thread="
	boost_thread$secondaryArchSuffix = $portVersion
	"

REQUIRES_thread="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_system$secondaryArchSuffix == $portVersion
	boost_chrono$secondaryArchSuffix == $portVersion
	"

PROVIDES_timer="
	boost_timer$secondaryArchSuffix = $portVersion
	"

REQUIRES_timer="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_chrono$secondaryArchSuffix == $portVersion
	boost_system$secondaryArchSuffix == $portVersion
	"

PROVIDES_wave="
	boost_wave$secondaryArchSuffix = $portVersion
	"

REQUIRES_wave="
	haiku$secondaryArchSuffix >= $haikuVersion
	boost_chrono$secondaryArchSuffix == $portVersion
	boost_date_time$secondaryArchSuffix == $portVersion
	boost_filesystem$secondaryArchSuffix == $portVersion
	boost_system$secondaryArchSuffix == $portVersion
	boost_thread$secondaryArchSuffix == $portVersion
	"
